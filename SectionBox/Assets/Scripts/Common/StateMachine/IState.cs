﻿// <copyright file="IState.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>12/5/2018 13:30:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, December 5, 2018

namespace VuxtaStudio.Common.FiniteStateMachine
{
    /// <summary>
    /// Interface of State
    /// </summary>
    public interface IState
    {
        /// <summary>
        /// Calling when entering state
        /// </summary>
        void OnStateEnter();

        /// <summary>
        /// Calling when updating state
        /// </summary>
        void OnStateUpdate();

        /// <summary>
        /// Calling when leaving state
        /// </summary>
        void OnStateExit();
    }
}
