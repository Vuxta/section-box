using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VuxtaStudio.SectionBox.GizmoUI;

namespace VuxtaStudio.SectionBox
{
    public class SectionBoxTestRunner : MonoBehaviour
    {
        [SerializeField]
        private CameraGizmoPointer gizmoPointer;

        [SerializeField]
        private List<GameObject> m_TestObjects;

        private bool m_SectionBoxEnabled = true;

        // Start is called before the first frame update        
        private void Awake()
        {
            InitBounds();
            //Reinit gizmo pointer when bounds reinit
            gizmoPointer.enabled = false;
            gizmoPointer.enabled = true;
        }


        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                m_SectionBoxEnabled = !m_SectionBoxEnabled;
                SectionBoxController.SetSectionBoxEnable(m_SectionBoxEnabled);
            }
        }

        private void InitBounds()
        {
            SectionBoxController.ClearBounds();
            for (int i = 0; i < m_TestObjects.Count; i++)
            {
                SectionBoxController.AddToBounds(m_TestObjects[i]);
            }
            SectionBoxController.SetSectionBoxEnable(m_SectionBoxEnabled);
        }
    }
}
