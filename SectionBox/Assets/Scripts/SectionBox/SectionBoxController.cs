using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VuxtaStudio.SectionBox
{
    public static class SectionBoxController
    {
        private static int m_CubePositionKey = Shader.PropertyToID("_CubePosition");
        private static int m_CubeExtentKey = Shader.PropertyToID("_CubeExtent");
        private static int m_ClipEnableKey = Shader.PropertyToID("_EnableClip");

        private static bool m_IsBoundClear = true;
        private static Bounds m_CurrentBounds = new Bounds();

        public delegate void OnBoundsChanged(Bounds bounds);

        public static void SetSectionBoxEnable(bool onOff)
        {
            if (onOff)
                Shader.SetGlobalInt(m_ClipEnableKey, 1);
            else
                Shader.SetGlobalInt(m_ClipEnableKey, 0);
        }

        public static void SetSectionBox(Vector3 center, Vector3 extent)
        {
            Shader.SetGlobalVector(m_CubePositionKey, center);
            Shader.SetGlobalVector(m_CubeExtentKey, extent);

            m_CurrentBounds.center = center;
            m_CurrentBounds.extents = extent;
        }

        public static void ClearBounds()
        {
            m_CurrentBounds = new Bounds();
            m_IsBoundClear = true;
        }

        public static void AddToBounds(GameObject go)
        {
            MeshRenderer[] meshRenderers = go.GetComponentsInChildren<MeshRenderer>();

            if (meshRenderers == null || meshRenderers.Length <= 0)
                return;

            for (int i = 0; i < meshRenderers.Length; i++)
            {
                if (m_IsBoundClear)
                {
                    m_CurrentBounds = meshRenderers[i].bounds;
                    m_IsBoundClear = false;
                }
                else
                    m_CurrentBounds.Encapsulate(meshRenderers[i].bounds);
            }

            SetSectionBox(m_CurrentBounds.center, m_CurrentBounds.extents);
        }

        public static Bounds GetBounds()
        {
            return m_CurrentBounds;
        }

        public static bool IsBoundClear()
        {
            return m_IsBoundClear;
        }
    }
}
