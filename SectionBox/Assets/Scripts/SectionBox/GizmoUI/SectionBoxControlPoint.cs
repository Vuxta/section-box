﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace VuxtaStudio.SectionBox.GizmoUI
{
    public class SectionBoxControlPoint : MonoBehaviour
    {
        [SerializeField]
        MeshRenderer m_Renderer;

        [SerializeField]
        Collider m_Collider;

        public void SetColor(Color m_Color)
        {
            m_Renderer.material.color = m_Color;
        }

        public void EnableRender(bool onOff)
        {
            m_Renderer.enabled = onOff;
        }

        public void EnableCollider(bool onOff)
        {
            m_Collider.enabled = onOff;
        }

        public bool IsColliderEqual(Collider col)
        {
            return (m_Collider == col);
        }
    }
}
