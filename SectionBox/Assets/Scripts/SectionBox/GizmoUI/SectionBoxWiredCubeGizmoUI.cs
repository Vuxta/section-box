﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace VuxtaStudio.SectionBox.GizmoUI
{
    public class SectionBoxWiredCubeGizmoUI : SectionBoxGizmoUIBase
    {
        [SerializeField]
        private Transform m_XAxisTransform;
        [SerializeField]
        private Transform m_XNegativeAxisTransform;
        [SerializeField]
        private Transform m_YAxisTransform;
        [SerializeField]
        private Transform m_YNegativeAxisTransform;
        [SerializeField]
        private Transform m_ZAxisTransform;
        [SerializeField]
        private Transform m_ZNegativeAxisTransform;

        [SerializeField]
        private SectionBoxControlPoint m_XAxisControlPoint;
        [SerializeField]
        private SectionBoxControlPoint m_XNegativeAxisControlPoint;
        [SerializeField]
        private SectionBoxControlPoint m_YAxisControlPoint;
        [SerializeField]
        private SectionBoxControlPoint m_YNegativeAxisControlPoint;
        [SerializeField]
        private SectionBoxControlPoint m_ZAxisControlPoint;
        [SerializeField]
        private SectionBoxControlPoint m_ZNegativeAxisControlPoint;

        [SerializeField]
        private LineRenderer m_WiredCubeRender;

        [SerializeField]
        private MeshRenderer m_TransparentCubeRender;

        [SerializeField]
        private float m_ScaleFactor = 0.1f;

        [SerializeField]
        private Color m_Color = new Color(0, 1, 0, 0.8f);

        [SerializeField]
        private Color m_SelectedColor = new Color(1, 1, 0, 0.8f);

        [SerializeField]
        private Color m_HoverColor = new Color(1, .75f, 0, 0.8f);

        private bool m_Enable = true;

        private void Awake()
        {
            m_XAxisControlPoint.transform.SetParent(null);
            m_XNegativeAxisControlPoint.transform.SetParent(null);
            m_YAxisControlPoint.transform.SetParent(null);
            m_YNegativeAxisControlPoint.transform.SetParent(null);
            m_ZAxisControlPoint.transform.SetParent(null);
            m_ZNegativeAxisControlPoint.transform.SetParent(null);

            m_XAxisControlPoint.SetColor(m_Color);
            m_XNegativeAxisControlPoint.SetColor(m_Color);
            m_YAxisControlPoint.SetColor(m_Color);
            m_YNegativeAxisControlPoint.SetColor(m_Color);
            m_ZAxisControlPoint.SetColor(m_Color);
            m_ZNegativeAxisControlPoint.SetColor(m_Color);

            m_WiredCubeRender.material.color = m_Color;
        }

        public override bool IsEnable()
        {
            return m_Enable;
        }

        public override void SetEnable(bool onOff)
        {
            m_Enable = onOff;
            m_XAxisControlPoint.SetColor(m_Color);
            m_XNegativeAxisControlPoint.SetColor(m_Color);
            m_YAxisControlPoint.SetColor(m_Color);
            m_YNegativeAxisControlPoint.SetColor(m_Color);
            m_ZAxisControlPoint.SetColor(m_Color);
            m_ZNegativeAxisControlPoint.SetColor(m_Color);

            m_XAxisControlPoint.EnableRender(onOff);
            m_XNegativeAxisControlPoint.EnableRender(onOff);
            m_YAxisControlPoint.EnableRender(onOff);
            m_YNegativeAxisControlPoint.EnableRender(onOff);
            m_ZAxisControlPoint.EnableRender(onOff);
            m_ZNegativeAxisControlPoint.EnableRender(onOff);

            m_WiredCubeRender.enabled = onOff;
            m_TransparentCubeRender.enabled = onOff;

            m_XAxisControlPoint.EnableCollider(onOff);
            m_XNegativeAxisControlPoint.EnableCollider(onOff);
            m_YAxisControlPoint.EnableCollider(onOff);
            m_YNegativeAxisControlPoint.EnableCollider(onOff);
            m_ZAxisControlPoint.EnableCollider(onOff);
            m_ZNegativeAxisControlPoint.EnableCollider(onOff);
        }

        public override bool SetHover(RaycastHit hitInfo)
        {
            if (!m_Enable)
            {
                return false;
            }

            m_XAxisControlPoint.SetColor(m_Color);
            m_XNegativeAxisControlPoint.SetColor(m_Color);
            m_YAxisControlPoint.SetColor(m_Color);
            m_YNegativeAxisControlPoint.SetColor(m_Color);
            m_ZAxisControlPoint.SetColor(m_Color);
            m_ZNegativeAxisControlPoint.SetColor(m_Color);

            if (m_XAxisControlPoint.IsColliderEqual(hitInfo.collider))
            {
                m_XAxisControlPoint.SetColor(m_HoverColor);
                return true;
            }

            if (m_XNegativeAxisControlPoint.IsColliderEqual(hitInfo.collider))
            {
                m_XNegativeAxisControlPoint.SetColor(m_HoverColor);
                return true;
            }

            if (m_YAxisControlPoint.IsColliderEqual(hitInfo.collider))
            {
                m_YAxisControlPoint.SetColor(m_HoverColor);
                return true;
            }

            if (m_YNegativeAxisControlPoint.IsColliderEqual(hitInfo.collider))
            {
                m_YNegativeAxisControlPoint.SetColor(m_HoverColor);
                return true;
            }

            if (m_ZAxisControlPoint.IsColliderEqual(hitInfo.collider))
            {
                m_ZAxisControlPoint.SetColor(m_HoverColor);
                return true;
            }

            if (m_ZNegativeAxisControlPoint.IsColliderEqual(hitInfo.collider))
            {
                m_ZNegativeAxisControlPoint.SetColor(m_HoverColor);
                return true;
            }

            return false;
        }

        public override void SetHoverExit()
        {
            m_XAxisControlPoint.SetColor(m_Color);
            m_XNegativeAxisControlPoint.SetColor(m_Color);
            m_YAxisControlPoint.SetColor(m_Color);
            m_YNegativeAxisControlPoint.SetColor(m_Color);
            m_ZAxisControlPoint.SetColor(m_Color);
            m_ZNegativeAxisControlPoint.SetColor(m_Color);
        }

        public override bool SetSelectGizmo(RaycastHit hitInfo, out GizmoAxis gizmoAxis)
        {
            gizmoAxis = GizmoAxis.None;

            if (!m_Enable)
            {
                return false;
            }

            m_XAxisControlPoint.SetColor(m_Color);
            m_XNegativeAxisControlPoint.SetColor(m_Color);
            m_YAxisControlPoint.SetColor(m_Color);
            m_YNegativeAxisControlPoint.SetColor(m_Color);
            m_ZAxisControlPoint.SetColor(m_Color);
            m_ZNegativeAxisControlPoint.SetColor(m_Color);

            if (m_XAxisControlPoint.IsColliderEqual(hitInfo.collider))
            {
                m_XAxisControlPoint.SetColor(m_SelectedColor);
                gizmoAxis = GizmoAxis.X;
                return true;
            }

            if (m_XNegativeAxisControlPoint.IsColliderEqual(hitInfo.collider))
            {
                m_XNegativeAxisControlPoint.SetColor(m_SelectedColor);
                gizmoAxis = GizmoAxis.XNegative;
                return true;
            }

            if (m_YAxisControlPoint.IsColliderEqual(hitInfo.collider))
            {
                m_YAxisControlPoint.SetColor(m_SelectedColor);
                gizmoAxis = GizmoAxis.Y;
                return true;
            }

            if (m_YNegativeAxisControlPoint.IsColliderEqual(hitInfo.collider))
            {
                m_YNegativeAxisControlPoint.SetColor(m_SelectedColor);
                gizmoAxis = GizmoAxis.YNegative;
                return true;
            }

            if (m_ZAxisControlPoint.IsColliderEqual(hitInfo.collider))
            {
                m_ZAxisControlPoint.SetColor(m_SelectedColor);
                gizmoAxis = GizmoAxis.Z;
                return true;
            }

            if (m_ZNegativeAxisControlPoint.IsColliderEqual(hitInfo.collider))
            {
                m_ZNegativeAxisControlPoint.SetColor(m_SelectedColor);
                gizmoAxis = GizmoAxis.ZNegative;
                return true;
            }

            return false;
        }

        private void Update()
        {
            if (!m_Enable)
                return;

            m_XAxisControlPoint.transform.position = m_XAxisTransform.position;
            m_XNegativeAxisControlPoint.transform.position = m_XNegativeAxisTransform.position;
            m_YAxisControlPoint.transform.position = m_YAxisTransform.position;
            m_YNegativeAxisControlPoint.transform.position = m_YNegativeAxisTransform.position;
            m_ZAxisControlPoint.transform.position = m_ZAxisTransform.position;
            m_ZNegativeAxisControlPoint.transform.position = m_ZNegativeAxisTransform.position;

            m_XAxisControlPoint.transform.localScale = Vector3.one * GetDistanceMultiplier(m_XAxisTransform.position) * m_ScaleFactor;
            m_XNegativeAxisControlPoint.transform.localScale = Vector3.one * GetDistanceMultiplier(m_XNegativeAxisTransform.position) * m_ScaleFactor;
            m_YAxisControlPoint.transform.localScale = Vector3.one * GetDistanceMultiplier(m_YAxisTransform.position) * m_ScaleFactor;
            m_YNegativeAxisControlPoint.transform.localScale = Vector3.one * GetDistanceMultiplier(m_YNegativeAxisTransform.position) * m_ScaleFactor;
            m_ZAxisControlPoint.transform.localScale = Vector3.one * GetDistanceMultiplier(m_ZAxisTransform.position) * m_ScaleFactor;
            m_ZNegativeAxisControlPoint.transform.localScale = Vector3.one * GetDistanceMultiplier(m_ZNegativeAxisTransform.position) * m_ScaleFactor;
        }
        private float GetDistanceMultiplier(Vector3 position)
        {
            return Mathf.Max(.01f, Mathf.Abs(VectorHelper.MagnitudeInDirection(transform.position - Camera.main.transform.position, Camera.main.transform.forward)));
        }
    }
}