﻿using System;
using UnityEngine;
using VuxtaStudio.Common.FiniteStateMachine;

namespace VuxtaStudio.SectionBox.GizmoUI
{
    public abstract class HoverEnterStateBase : IState
    {
        protected LayerMask m_GizmoLayerMask;
        protected Action<RaycastHit> m_OnHoverEnterCallback;

        protected abstract Ray m_SelectObjectRay { get; }
        protected abstract bool m_SelectObjectButtonDown { get; }

        public void Initialize(LayerMask gizmoLayerMask,
                                             Action<RaycastHit> onHoverEnter)
        {
            m_GizmoLayerMask = gizmoLayerMask;
            m_OnHoverEnterCallback = onHoverEnter;
        }

        public virtual void OnStateEnter() { }

        public virtual void OnStateExit() { }

        public virtual void OnStateUpdate()
        {
            RaycastHit hitInfo;

            if (Physics.Raycast(m_SelectObjectRay, out hitInfo, Mathf.Infinity, m_GizmoLayerMask))
            {
                m_OnHoverEnterCallback(hitInfo);
                return;
            }
        }
    }
}
