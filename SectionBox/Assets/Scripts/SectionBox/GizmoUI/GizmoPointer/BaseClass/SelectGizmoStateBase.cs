﻿using System;
using UnityEngine;
using VuxtaStudio.Common.FiniteStateMachine;

namespace VuxtaStudio.SectionBox.GizmoUI
{
    public abstract class SelectGizmoStateBase : IState
    {
        protected Action<Ray> m_OnUpdatePointerRayCallback;
        protected Action m_OnReleaseGizmoCallback;

        protected abstract Ray m_SelectObjectRay { get; }
        protected abstract bool m_SelectButtonPressed { get; }

        public void Initialize(Action onReleaseGizmo, Action<Ray> onUpdatePointerRay)
        {
            m_OnReleaseGizmoCallback = onReleaseGizmo;
            m_OnUpdatePointerRayCallback = onUpdatePointerRay;
        }

        public virtual void OnStateEnter() { }

        public virtual void OnStateExit() { }

        public virtual void OnStateUpdate()
        {
            if (m_SelectButtonPressed)
            {
                m_OnUpdatePointerRayCallback(m_SelectObjectRay);
            }
            else
            {
                m_OnReleaseGizmoCallback();
            }
        }
    }
}
