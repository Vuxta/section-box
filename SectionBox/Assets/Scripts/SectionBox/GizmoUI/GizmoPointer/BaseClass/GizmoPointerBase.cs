﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using VuxtaStudio.Common.FiniteStateMachine;

namespace VuxtaStudio.SectionBox.GizmoUI
{
    public abstract class GizmoPointerBase : MonoBehaviour
    {
        [SerializeField]
        private GameObject m_RuntimeGizmoPrefab;

        protected abstract HoverEnterStateBase m_MouseHoverEnterState { get; }
        protected abstract HoverStateBase m_MouseHoverState { get; }
        protected abstract SelectGizmoStateBase m_MouseSelectGizmoState { get; }

        private GameObject m_RuntimeGizmoObject;
        private SectionBoxGizmo m_RuntimeGizmo;

        protected LayerMask m_GizmoLayerMask;

        private StateMachine m_StateMachine;


        private void Awake()
        {
            m_RuntimeGizmoObject = Instantiate(m_RuntimeGizmoPrefab);
            m_RuntimeGizmo = m_RuntimeGizmoObject.GetComponent<SectionBoxGizmo>();
            m_GizmoLayerMask = 1 << m_RuntimeGizmo.GetGizmoLayerMask();

            m_MouseHoverEnterState.Initialize(m_GizmoLayerMask, OnHoverEnter);
            m_MouseHoverState.Initialize(m_GizmoLayerMask, OnHoverExit, OnSelectGizmo);
            m_MouseSelectGizmoState.Initialize(OnReleaseGizmo, OnUpdatePointerRay);

            m_StateMachine = new StateMachine();
            m_StateMachine.ChangeState(m_MouseHoverEnterState);
        }

        private void OnDisable()
        {
            m_RuntimeGizmo.SetGizmoDisable();
        }

        private void OnEnable()
        {
            m_RuntimeGizmo.SetGizmoEnable();
        }

        protected void OnUpdatePointerRay(Ray obj)
        {
            m_RuntimeGizmo.UpdatePointerRay(obj);
        }

        protected void OnReleaseGizmo()
        {
            m_RuntimeGizmo.ReleaseGizmo();
            m_StateMachine.ChangeState(m_MouseHoverState);
        }

        protected void OnSelectGizmo(RaycastHit obj, Ray pointerRay)
        {
            if (m_RuntimeGizmo.SetSelectGizmo(obj, pointerRay))
            {
                m_StateMachine.ChangeState(m_MouseSelectGizmoState);
            }
        }

        protected void OnHoverExit()
        {
            m_RuntimeGizmo.SetHoverExit();
            m_StateMachine.ChangeState(m_MouseHoverEnterState);
        }

        protected void OnHoverEnter(RaycastHit obj)
        {
            m_RuntimeGizmo.SetHover(obj);
            m_MouseHoverState.SetTarget(obj.collider);
            m_StateMachine.ChangeState(m_MouseHoverState);
        }

        private void Update()
        {
            m_StateMachine.OnMachineUpdate();
        }
    }
}
