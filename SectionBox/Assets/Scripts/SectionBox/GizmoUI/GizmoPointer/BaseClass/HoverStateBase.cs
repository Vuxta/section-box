﻿using System;
using UnityEngine;
using VuxtaStudio.Common.FiniteStateMachine;

namespace VuxtaStudio.SectionBox.GizmoUI
{
    public abstract class HoverStateBase : IState
    {
        protected LayerMask m_GizmoLayerMask;
        protected Action m_OnHoverExitCallback;
        protected Action<RaycastHit, Ray> m_OnSelectGizmoCallback;

        protected Collider m_HoverTarget;

        protected abstract Ray m_SelectObjectRay { get; }
        protected abstract bool m_SelectObjectButtonDown { get; }

        public void Initialize(LayerMask gizmoLayerMask, Action onHoverExit, Action<RaycastHit, Ray> onSelectGizmo)
        {
            m_GizmoLayerMask = gizmoLayerMask;
            m_OnHoverExitCallback = onHoverExit;
            m_OnSelectGizmoCallback = onSelectGizmo;
        }

        public void SetTarget(Collider target)
        {
            m_HoverTarget = target;
        }

        public virtual void OnStateEnter() { }

        public virtual void OnStateExit() { }

        public virtual void OnStateUpdate()
        {
            RaycastHit hitInfo;
            Ray pointerRay = m_SelectObjectRay;
            if (!Physics.Raycast(pointerRay, out hitInfo, Mathf.Infinity, m_GizmoLayerMask)
                || hitInfo.collider != m_HoverTarget)
            {
                m_OnHoverExitCallback();
                return;
            }

            if (m_SelectObjectButtonDown)
            {
                m_OnSelectGizmoCallback(hitInfo, pointerRay);
                return;
            }
        }
    }
}
