﻿using UnityEngine;

namespace VuxtaStudio.SectionBox.GizmoUI
{
    public abstract class SectionBoxGizmoUIBase : MonoBehaviour
    {
        public abstract bool IsEnable();
        public abstract void SetEnable(bool onOff);
        public abstract bool SetHover(RaycastHit hitInfo);
        public abstract bool SetSelectGizmo(RaycastHit hitInfo, out GizmoAxis gizmoAxis);
        public abstract void SetHoverExit();

        public virtual void ReleaseGizmo() { }
    }
}
