﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VuxtaStudio.SectionBox.GizmoUI
{
    public enum GizmoAxis
    {
        None,
        X,
        XNegative,
        Y,
        YNegative,
        Z,
        ZNegative,
    }

    public class SectionBoxGizmo : MonoBehaviour
    {
        [SerializeField]
        private SectionBoxGizmoUIBase m_TranslateGizmo;

        private bool m_Enable = false;
        private bool m_IsGizmoSelect = false;

        private GizmoAxis m_RunningGizmoAxis = GizmoAxis.None;
        private Ray m_CurrentPointerRay;
        private Vector3 m_OperationPlaneNormal;
        private Vector3 m_OperationOrigin;
        private Vector3 m_ProjectedAxis;
        private Vector3 m_ObjectWorkingAixs;
        private Vector3 m_MousePositionOnOperationPlane;
        private Vector3 m_PreviousMousePositionOnOperationPlane;
        private Vector3 m_TargetXAxis;
        private Vector3 m_TargetYAxis;
        private Vector3 m_TargetZAxis;

        private bool m_IsNegativeAxis = false;

        public void SetGizmoDisable()
        {
            m_TranslateGizmo.SetEnable(false);
            m_IsGizmoSelect = false;
            m_RunningGizmoAxis = GizmoAxis.None;
        }

        public void SetGizmoEnable()
        {
            if (SectionBoxController.IsBoundClear())
            {
                Debug.LogWarning("Please set initial bounds before enable gizmo");
            }

            Bounds currentBounds = SectionBoxController.GetBounds();
            transform.position = currentBounds.center;
            transform.localScale = currentBounds.size;

            m_Enable = true;
            m_TranslateGizmo.SetEnable(true);
            m_RunningGizmoAxis = GizmoAxis.None;
        }

        public void SetHover(RaycastHit hitInfo)
        {
            m_RunningGizmoAxis = GizmoAxis.None;

            m_TranslateGizmo.SetHover(hitInfo);
        }

        public void SetHoverExit()
        {
            m_RunningGizmoAxis = GizmoAxis.None;

            m_TranslateGizmo.SetHoverExit();
        }

        public bool SetSelectGizmo(RaycastHit hitInfo, Ray pointerRay)
        {
            GizmoAxis gizmoAxis = GizmoAxis.None;
            m_IsNegativeAxis = false;

            //Find out selected gizmo mode and axis, there should only be one running mode
            if (m_TranslateGizmo.SetSelectGizmo(hitInfo, out gizmoAxis))
            {
                m_RunningGizmoAxis = gizmoAxis;
            }
            else
            {
                return false;
            }

            m_IsGizmoSelect = true;

            //initialize the virtual working plane which will be used to calculate pointer movement
            m_CurrentPointerRay = pointerRay;
            m_OperationOrigin = transform.position;

            m_OperationPlaneNormal = m_CurrentPointerRay.origin - m_OperationOrigin;
            m_MousePositionOnOperationPlane = GeometryHelper.LinePlaneIntersect(m_CurrentPointerRay.origin, m_CurrentPointerRay.direction, m_OperationOrigin, m_OperationPlaneNormal);
            m_PreviousMousePositionOnOperationPlane = m_MousePositionOnOperationPlane;

            m_TargetXAxis = transform.right;
            m_TargetYAxis = transform.up;
            m_TargetZAxis = transform.forward;

            m_ObjectWorkingAixs = Vector3.zero;

            //By the axis selected by the user, we should define which axis we should be working on.            
            if (m_RunningGizmoAxis == GizmoAxis.X)
            {
                m_IsNegativeAxis = false;
                m_ObjectWorkingAixs = m_TargetXAxis;
            }
            else if (m_RunningGizmoAxis == GizmoAxis.Y)
            {
                m_IsNegativeAxis = false;
                m_ObjectWorkingAixs = m_TargetYAxis;
            }
            else if (m_RunningGizmoAxis == GizmoAxis.Z)
            {
                m_IsNegativeAxis = false;
                m_ObjectWorkingAixs = m_TargetZAxis;
            }
            else if (m_RunningGizmoAxis == GizmoAxis.XNegative)
            {
                m_IsNegativeAxis = true;
                m_ObjectWorkingAixs = -1f * m_TargetXAxis;
            }
            else if (m_RunningGizmoAxis == GizmoAxis.YNegative)
            {
                m_IsNegativeAxis = true;
                m_ObjectWorkingAixs = -1f * m_TargetYAxis;
            }
            else if (m_RunningGizmoAxis == GizmoAxis.ZNegative)
            {
                m_IsNegativeAxis = true;
                m_ObjectWorkingAixs = -1f * m_TargetZAxis;
            }

            m_ProjectedAxis = Vector3.ProjectOnPlane(m_ObjectWorkingAixs, m_OperationPlaneNormal).normalized;

            return true;
        }

        public void ReleaseGizmo()
        {
            m_TranslateGizmo.ReleaseGizmo();
            m_IsGizmoSelect = false;
            m_RunningGizmoAxis = GizmoAxis.None;
        }

        public void UpdatePointerRay(Ray pointerRay)
        {
            m_CurrentPointerRay = pointerRay;
        }

        public LayerMask GetGizmoLayerMask()
        {
            return gameObject.layer;
        }

        private void Update()
        {
            if (!m_Enable)
                return;

            if (!m_IsGizmoSelect)
                return;

            //Update mouse position
            m_MousePositionOnOperationPlane = GeometryHelper.LinePlaneIntersect(m_CurrentPointerRay.origin,
                                                                                                                                                    m_CurrentPointerRay.direction,
                                                                                                                                                    m_OperationOrigin,
                                                                                                                                                    m_OperationPlaneNormal);

            OnUpdateTranslate();

            m_PreviousMousePositionOnOperationPlane = m_MousePositionOnOperationPlane;
        }

        private void OnUpdateTranslate()
        {
            float amount = 0f;
            Vector3 movement = Vector3.zero;

            amount = VectorHelper.MagnitudeInDirection(m_MousePositionOnOperationPlane - m_PreviousMousePositionOnOperationPlane, m_ProjectedAxis);
            movement = m_ObjectWorkingAixs * amount;

            if (m_IsNegativeAxis)
            {
                transform.localScale -= movement;
            }
            else
            {
                transform.localScale += movement;
            }

            if (transform.localScale.x < 0f)
                transform.localScale = new Vector3(0f, transform.localScale.y, transform.localScale.z);
            else if (transform.localScale.y < 0f)
                transform.localScale = new Vector3(transform.localScale.x, 0f, transform.localScale.z);
            else if (transform.localScale.z < 0f)
                transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, 0f);
            else
                transform.Translate(movement * 0.5f, Space.World);

        }

        private void LateUpdate()
        {
            if (!m_Enable)
                return;

            if (!m_IsGizmoSelect)
                return;

            SectionBoxController.SetSectionBox(transform.position, transform.localScale * 0.5f);
        }

        private void OnApplicationQuit()
        {
            SectionBoxController.SetSectionBoxEnable(false);
            SectionBoxController.SetSectionBox(Vector3.zero, Vector3.one);
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireCube(transform.position, transform.localScale);
        }
    }
}

