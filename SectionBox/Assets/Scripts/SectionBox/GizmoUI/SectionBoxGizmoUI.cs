﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace VuxtaStudio.SectionBox.GizmoUI
{
    public class SectionBoxGizmoUI : SectionBoxGizmoUIBase
    {
        [SerializeField]
        private MeshRenderer m_XAxisRender;

        [SerializeField]
        private MeshRenderer m_XNegativeAxisRender;

        [SerializeField]
        private MeshRenderer m_YAxisRender;

        [SerializeField]
        private MeshRenderer m_YNegativeAxisRender;

        [SerializeField]
        private MeshRenderer m_ZAxisRender;

        [SerializeField]
        private MeshRenderer m_ZNegativeAxisRender;

        [SerializeField]
        private Collider m_XAxisCollider;

        [SerializeField]
        private Collider m_XNegativeAxisCollider;

        [SerializeField]
        private Collider m_YAxisCollider;

        [SerializeField]
        private Collider m_YNegativeAxisCollider;

        [SerializeField]
        private Collider m_ZAxisCollider;

        [SerializeField]
        private Collider m_ZNegativeAxisCollider;

        [SerializeField]
        private Color m_Color = new Color(0, 1, 0, 0.8f);

        [SerializeField]
        private Color m_SelectedColor = new Color(1, 1, 0, 0.8f);

        [SerializeField]
        private Color m_HoverColor = new Color(1, .75f, 0, 0.8f);

        private bool m_Enable = true;

        private void Awake()
        {
            m_XAxisRender.material.color = m_Color;
            m_XNegativeAxisRender.material.color = m_Color;
            m_YAxisRender.material.color = m_Color;
            m_YNegativeAxisRender.material.color = m_Color;
            m_ZAxisRender.material.color = m_Color;
            m_ZNegativeAxisRender.material.color = m_Color;
        }

        public override bool IsEnable()
        {
            return m_Enable;
        }

        public override void SetEnable(bool onOff)
        {
            m_Enable = onOff;
            m_XAxisRender.material.color = m_Color;
            m_XNegativeAxisRender.material.color = m_Color;
            m_YAxisRender.material.color = m_Color;
            m_YNegativeAxisRender.material.color = m_Color;
            m_ZAxisRender.material.color = m_Color;
            m_ZNegativeAxisRender.material.color = m_Color;

            m_XAxisRender.enabled = onOff;
            m_XNegativeAxisRender.enabled = onOff;
            m_YAxisRender.enabled = onOff;
            m_YNegativeAxisRender.enabled = onOff;
            m_ZAxisRender.enabled = onOff;
            m_ZNegativeAxisRender.enabled = onOff;

            m_XAxisCollider.enabled = onOff;
            m_XNegativeAxisCollider.enabled = onOff;
            m_YAxisCollider.enabled = onOff;
            m_YNegativeAxisCollider.enabled = onOff;
            m_ZAxisCollider.enabled = onOff;
            m_ZNegativeAxisCollider.enabled = onOff;
        }

        public override bool SetHover(RaycastHit hitInfo)
        {
            if (!m_Enable)
            {
                return false;
            }

            m_XAxisRender.material.color = m_Color;
            m_XNegativeAxisRender.material.color = m_Color;
            m_YAxisRender.material.color = m_Color;
            m_YNegativeAxisRender.material.color = m_Color;
            m_ZAxisRender.material.color = m_Color;
            m_ZNegativeAxisRender.material.color = m_Color;

            if (hitInfo.collider == m_XAxisCollider)
            {
                m_XAxisRender.material.color = m_HoverColor;
                return true;
            }

            if (hitInfo.collider == m_XNegativeAxisCollider)
            {
                m_XNegativeAxisRender.material.color = m_HoverColor;
                return true;
            }

            if (hitInfo.collider == m_YAxisCollider)
            {
                m_YAxisRender.material.color = m_HoverColor;
                return true;
            }

            if (hitInfo.collider == m_YNegativeAxisCollider)
            {
                m_YNegativeAxisRender.material.color = m_HoverColor;
                return true;
            }

            if (hitInfo.collider == m_ZAxisCollider)
            {
                m_ZAxisRender.material.color = m_HoverColor;
                return true;
            }

            if (hitInfo.collider == m_ZNegativeAxisCollider)
            {
                m_ZNegativeAxisRender.material.color = m_HoverColor;
                return true;
            }

            return false;
        }

        public override void SetHoverExit()
        {
            m_XAxisRender.material.color = m_Color;
            m_XNegativeAxisRender.material.color = m_Color;
            m_YAxisRender.material.color = m_Color;
            m_YNegativeAxisRender.material.color = m_Color;
            m_ZAxisRender.material.color = m_Color;
            m_ZNegativeAxisRender.material.color = m_Color;
        }

        public override bool SetSelectGizmo(RaycastHit hitInfo, out GizmoAxis gizmoAxis)
        {
            gizmoAxis = GizmoAxis.None;

            if (!m_Enable)
            {
                return false;
            }

            m_XAxisRender.material.color = m_Color;
            m_XNegativeAxisRender.material.color = m_Color;
            m_YAxisRender.material.color = m_Color;
            m_YNegativeAxisRender.material.color = m_Color;
            m_ZAxisRender.material.color = m_Color;
            m_ZNegativeAxisRender.material.color = m_Color;

            if (hitInfo.collider == m_XAxisCollider)
            {
                m_XAxisRender.material.color = m_SelectedColor;
                gizmoAxis = GizmoAxis.X;
                return true;
            }

            if (hitInfo.collider == m_XNegativeAxisCollider)
            {
                m_XNegativeAxisRender.material.color = m_SelectedColor;
                gizmoAxis = GizmoAxis.XNegative;
                return true;
            }

            if (hitInfo.collider == m_YAxisCollider)
            {
                m_YAxisRender.material.color = m_SelectedColor;
                gizmoAxis = GizmoAxis.Y;
                return true;
            }

            if (hitInfo.collider == m_YNegativeAxisCollider)
            {
                m_YNegativeAxisRender.material.color = m_SelectedColor;
                gizmoAxis = GizmoAxis.YNegative;
                return true;
            }

            if (hitInfo.collider == m_ZAxisCollider)
            {
                m_ZAxisRender.material.color = m_SelectedColor;
                gizmoAxis = GizmoAxis.Z;
                return true;
            }

            if (hitInfo.collider == m_ZNegativeAxisCollider)
            {
                m_ZNegativeAxisRender.material.color = m_SelectedColor;
                gizmoAxis = GizmoAxis.ZNegative;
                return true;
            }

            return false;
        }
    }
}